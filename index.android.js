/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
  Image,
  DeviceEventEmitter,
} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';

import {
  AccountManager,
  SignalConnectionService,
  SignalServiceMessagePipe,
  LibSignalsModule,
} from './android/humaniq-libsignals/main';

import {UtilsAndroid} from './UtilsAndroid'

const sender = "+79137698312";
const recipient = "+79137698313";

const registrationId = 13992;
const signalingKey = "/S5f0EvU3k2XB1u6wje9sVtFK0iq68qMehR8rKxslEv9xeBq1De0mKaFm778aoo6BJBYAw==";

const user1 = {
  _id: 1,
  name: 'User 1',
  avatar: 'http://findicons.com/files/icons/1072/face_avatars/300/a02.png',
};

const user2 = {
  _id: 2,
  name: 'User 2',
  avatar: 'http://icons.iconarchive.com/icons/hopstarter/face-avatars/256/Female-Face-FB-1-icon.png',
};

const VIEW_REGISTER = 0;
const VIEW_CHAT = 1;

export default class example extends Component {
  constructor(props) {
    super(props);
    this.state = {verifyValue: '', view: VIEW_REGISTER, messages: []};
  }

  render() {
    switch (this.state.view) {
      case VIEW_REGISTER:
        return this.renderReg();

      case VIEW_CHAT:
        return this.renderChat();

      default:
        throw "Unspecified view"
    }
  }

  componentWillMount() {
    DeviceEventEmitter.addListener(
      'signalMessageReceived',
      function(e: Event) {
        console.log(e);
      }
    );
  }

  onSend = (messages = []) => {
    console.log("SEND: ", messages);
    this.signalConnectionService.sendMessage(recipient, messages[0].text, []).then(() => {
      console.log("message was sent");
      this.setState((previousState) => {
        return {messages: GiftedChat.append(previousState.messages, messages)};
      });
    });
  };

  renderReg = () => (
    <View style={styles.container}>
      <Text style={styles.welcome}>Welcome to Chat Testing!</Text>
      <Button
        title="Register"
        style={styles.regularButton}
        onPress={async () => {
          this.accountManager = await AccountManager.createInstance(
            'https://beta-api.humaniq.co/signal',
            sender,
            '123456',
            'OWF',
          );
          console.log('created account', this.accountManager);

          const {registrationId, signalingKey} = await this.accountManager.registration();
          await this.accountManager.initAccount();

          console.log("registered", registrationId, signalingKey);

          this.signalConnectionService =
            await SignalConnectionService.createInstance(this.accountManager);

          console.log("created connection service");

          await this.signalConnectionService.startMessageRetrievalService();
          console.log("started message retrieval service");

          console.log("account manager data", this.accountManager);

          this.setState({view: VIEW_CHAT});
          this.mockChat();
        }}
      />

      <Button
        title="Login"
        style={styles.regularButton}
        onPress={async () => {
          this.accountManager = await AccountManager.createInstance(
            'https://beta-api.humaniq.co/signal',
            sender,
            '123456',
            'OWF',
          );

          await this.accountManager.loadKeys(registrationId, signalingKey);
          await this.accountManager.initAccount();

          console.log("account was init");

          this.signalConnectionService =
            await SignalConnectionService.createInstance(this.accountManager);

          console.log("created connection service");

          await this.signalConnectionService.startMessageRetrievalService();
          console.log("started message retrieval service");

          this.setState({view: VIEW_CHAT});
          this.mockChat();
        }}
      />
    </View>
  );

  renderChat = () => (
    <View style={{backgroundColor: '#ffffff', flex: 1}}>
      <GiftedChat
        renderAvatarOnTop
        messages={this.state.messages}
        onSend={this.onSend}
        user={user1}
      />
    </View>
  );

  mockChat() {
    this.setState({
      messages: [
        {
          _id: 5,
          text: 'Давай к нам',
          createdAt: new Date(Date.UTC(2017, 5, 30, 5, 27, 31)),
          user: user2,
        },
        {
          _id: 4,
          text: 'Фигасе!!!!',
          createdAt: new Date(Date.UTC(2017, 5, 30, 5, 25, 16)),
          user: user1,
        },
        {
          _id: 3,
          text: 'Глянь где я',
          image: 'https://files1.adme.ru/files/news/part_79/793310/10095010-797ab841d30ecf2e893c6ff55e0e067a_970x-1000-224ec000e1-1484579184.jpg',
          createdAt: new Date(Date.UTC(2017, 5, 30, 5, 22, 50)),
          user: user2,
        },
        {
          _id: 1,
          text: 'Привет',
          createdAt: new Date(Date.UTC(2017, 5, 30, 5, 20, 24)),
          user: user1,
        },
        {
          _id: 2,
          text: 'Привет. Как у тебя дела?',
          createdAt: new Date(Date.UTC(2017, 5, 30, 5, 19, 33)),
          user: user2,
        },
      ]
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  regularButton: {
    marginTop: 10,
    marginBottom: 10,
  },
  regularTextInput: {
    height: 40,
    width: 100,
    borderColor: 'gray',
    borderWidth: 1,
    marginTop: 10,
    marginBottom: 10,
  },
  navbar: {
    height: 40,
    position: 'absolute',
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    zIndex: 2,
    borderColor: '#9b9b9b',
    borderBottomWidth: 1,
  },
  userPhoto: {
    alignSelf: 'flex-end',
    padding: 10,
  },
  phoneNumber: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  closeBtn: {
    padding: 10,
    alignSelf: 'flex-start',
  },
  camera: {
    flex: 1,
  },
  previewImage: {
    flex: 1,
  },
  captureContainer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    height: 70,
  },
  captureBtn: {
    borderRadius: 55,
    borderWidth: 5,
  },
  uploadBtn: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

AppRegistry.registerComponent('example', () => example);
// AppRegistry.registerHeadlessTask('ReadMessages', () => require('./ReadMessages'));
