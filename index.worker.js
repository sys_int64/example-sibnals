import {WorkerService} from 'rn-workers'

const worker = new WorkerService();
worker.onmessage = message => {
  //Reply the message back to app
  console.log("received in worker: " + message);
  worker.postMessage("Hello from the other side (" + message + ")")
};
